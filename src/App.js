import React, {useState} from 'react';
import {AppProvider, Form, FormLayout, Page} from "@shopify/polaris";
import "@shopify/polaris/styles.css";
import GoogleFontInput from "./components/GoogleFontInput/GoogleFontInput";

function App() {
  const [currentFont, setCurrentFont] = useState("Roboto");

  return (
    <AppProvider i18n={{}}>
      <Page>
        <Form>
          <FormLayout>
            <GoogleFontInput
              value={currentFont}
              onSelect={(val) => setCurrentFont(val)}
            />
          </FormLayout>
        </Form>
      </Page>
    </AppProvider>
  );
}

export default App;
