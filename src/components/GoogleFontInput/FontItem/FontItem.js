import React, {useEffect} from "react";
import PropTypes from 'prop-types';
import {Button, Card} from "@shopify/polaris";
import "./FontItem.scss"
import {AddMajorMonotone} from "@shopify/polaris-icons";

const FontItem = ({label, name, onSelect}) => {
  useEffect(() => {
    const linkElement = document.createElement("link");
    linkElement.rel = "stylesheet";
    linkElement.media = "all";
    linkElement.href = `https://fonts.googleapis.com/css?family=${name}`;
    document.head.appendChild(linkElement);
  }, []);

  return (
    <div className={'Avada-FontManager__Wrapper'}>
      <Card title={label} actions={[{content: (
          <Button plain icon={AddMajorMonotone} onClick={() => onSelect(name)}>Select</Button>
        )}]}>
        <Card.Section>
          <p style={{
            fontFamily: name.replace(/\+/g, ' '),
            lineHeight: "26px",
            fontSize: "26px"
          }}>
            Lorem ipsum dolor sit amet, consectetur adipiscing elit
          </p>
        </Card.Section>
      </Card>
    </div>
  )
};

FontItem.propTypes = {
  /** Handler when select a font */
  onSelect: PropTypes.func,
  /** Font name */
  label: PropTypes.string,
  /** Font value in form Roboto+Condensed */
  name: PropTypes.string
};

export default FontItem;
