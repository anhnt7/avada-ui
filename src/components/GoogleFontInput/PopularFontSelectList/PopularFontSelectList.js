import React, {useCallback, useState} from 'react';
import {ActionList, Icon, Labelled, Popover, TextStyle} from '@shopify/polaris';
import * as PropTypes from 'prop-types';
import {SelectMinor} from "@shopify/polaris-icons";
import {popularFonts} from "../options/popularFonts";

/**
 *
 * @param value
 * @param onSelect
 * @param label
 * @returns {*}
 * @constructor
 */
export default function PopularFontSelectList({value, onSelect, label}) {
  const [popoverActive, setPopoverActive] = useState(false);
  const [loadedFonts, setLoadedFonts] = useState(false);

  const togglePopoverActive = useCallback(
    () => setPopoverActive(popoverActive => !popoverActive),
    []
  );

  const loadGoogleFonts = () => {
    if (!loadedFonts) {
      const link = document.createElement('link');
      let fonts = popularFonts.map(font => font.value);
      fonts = fonts.join('|');
      link.rel = 'stylesheet';
      link.href = `https://fonts.googleapis.com/css?family=${fonts}&display=swap`;
      document.head.appendChild(link);
      setLoadedFonts(true);
    }
  };

  const activator = (
    <div
      style={{
        minWidth: '200px',
        height: '35px',
        display: 'flex',
        justifyContent: 'center',
        alignItems: 'center',
        cursor: 'pointer',
        marginRight: '15px',
        marginBottom: '15px',
        border: "0.1rem solid var(--p-border-on-surface, #c4cdd5)",
        borderRadius: "3px",
        boxShadow: "inset 0 1px 0 0 rgba(99, 115, 129, 0.05)",
        padding: "1.8rem"
      }}
      onClick={() => {
        togglePopoverActive();
        loadGoogleFonts();
      }}
    >
      <div style={{display: 'flex', width: "100%"}}>
        <div style={{width: "90%", fontFamily: value.replace(/\+/g, ' ')}}>
          <TextStyle>
            {value.replace(/\+/g, ' ')}
          </TextStyle>
        </div>
        <Icon source={SelectMinor}/>
      </div>
    </div>
  );

  return (
    <Labelled label={label} id={'font'}>
      <div style={{display: 'flex', flexWrap: 'wrap'}}>
        <Popover
          active={popoverActive}
          activator={activator}
          onClose={togglePopoverActive}
        >
          <ActionList
            items={popularFonts.map(font => ({
              content: <div style={{fontFamily: font.label}}>{font.label}</div>,
              onAction: () => onSelect(font.value)
            }))}
          />
        </Popover>
      </div>
    </Labelled>
  );
}

PopularFontSelectList.propTypes = {
  /** Handler when select a font */
  onSelect: PropTypes.func,
  /** The current selected font */
  value: PropTypes.string
};
