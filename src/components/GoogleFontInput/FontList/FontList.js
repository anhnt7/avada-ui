import React, {useState} from 'react';
import {Autocomplete, Button, Icon, Scrollable, Stack} from "@shopify/polaris";
import PropTypes from 'prop-types';
import {SearchMinor} from "@shopify/polaris-icons";
import FontItem from "../FontItem/FontItem";
import allFonts from "../options/fonts";

const FontList = ({onSelect}) => {
  const [cursor, setCursor] = useState(0);
  const [fontItems, setFontItems] = useState(allFonts.slice(0, 20));
  const [inputValue, setInputValue] = useState('');

  const handleFilter = () => {
    const filterRegex = new RegExp(inputValue, 'i');
    setFontItems(allFonts.filter(option => option.label.match(filterRegex)));
  };

  return (
    <Stack vertical>
      <Stack.Item fill>
        <Stack>
          <Stack.Item fill>
            <Autocomplete.TextField
              onChange={(val) => setInputValue(val)}
              label="Search font"
              value={inputValue}
              prefix={<Icon source={SearchMinor} color="inkLighter"/>}
              placeholder="Search"
            />
          </Stack.Item>
          <div style={{marginTop: "2.2rem"}}>
            <Button onClick={handleFilter}>
              Search
            </Button>
          </div>
        </Stack>

      </Stack.Item>
      <Scrollable
        shadow
        style={{height: '500px'}}
        onScrolledToBottom={() => {
          const nextCursor = cursor + 1;
          setFontItems(allFonts.slice(0, nextCursor * 20));
          setCursor(nextCursor);
        }}
      >
        <Stack>
          {fontItems.map(font => (
            <FontItem label={font.label} name={font.value} onSelect={onSelect}/>
          ))}
        </Stack>
      </Scrollable>
    </Stack>
  )
};

FontList.propTypes = {
  onSelect: PropTypes.func
};

export default FontList;
