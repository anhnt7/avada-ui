##### Google Font Input

### Description
This component provides an input to select from some most popular fonts along with a "More fonts" button to select over 1k Google Fonts. 

### Highlight
- This component does not load all the CSS of all fonts at once but only when the font is scrolled to.
- Search interface
- Visual UI
### Proptypes
| Name | Type | Required | Default | Description |
|:-----|:-----|:-----|:-----|:-----|
|onSelect|function|true|-|Handler when select a font|
|value|string|true|-|The current selected font|
|label|function|false|Font family|The label for the input. Default is "Font family"|

### Snippets
```js
import React, {useState} from 'react';
import {AppProvider, Modal, Page, Form, FormLayout} from "@shopify/polaris";
import "@shopify/polaris/styles.css";
import GoogleFontInput from '@bit/avada.avada_ui.google-font-input';

function App() {
  const [currentFont, setCurrentFont] = useState("Roboto");

  return (
    <AppProvider i18n={{}}>
      <Page>
        <Form>
          <FormLayout>
            <GoogleFontInput
              value={currentFont}
              onSelect={(val) => setCurrentFont(val)}
              label={""}
            />
          </FormLayout>
        </Form>
      </Page>
    </AppProvider>
  );
}

export default App;
```
