const allFonts = [
  {
    label: 'Roboto',
    value: 'Roboto'
  },
  {
    label: 'Open Sans',
    value: 'Open+Sans'
  },
  {
    label: 'Lato',
    value: 'Lato'
  },
  {
    label: 'Montserrat',
    value: 'Montserrat'
  },
  {
    label: 'Roboto Condensed',
    value: 'Roboto+Condensed'
  },
  {
    label: 'Source Sans Pro',
    value: 'Source+Sans+Pro'
  },
  {
    label: 'Oswald',
    value: 'Oswald'
  },
  {
    label: 'Roboto Mono',
    value: 'Roboto+Mono'
  },
  {
    label: 'Raleway',
    value: 'Raleway'
  },
  {
    label: 'Poppins',
    value: 'Poppins'
  },
  {
    label: 'Roboto Slab',
    value: 'Roboto+Slab'
  },
  {
    label: 'Noto Sans',
    value: 'Noto+Sans'
  },
  {
    label: 'Merriweather',
    value: 'Merriweather'
  },
  {
    label: 'PT Sans',
    value: 'PT+Sans'
  },
  {
    label: 'Playfair Display',
    value: 'Playfair+Display'
  },
  {
    label: 'Ubuntu',
    value: 'Ubuntu'
  },
  {
    label: 'Muli',
    value: 'Muli'
  },
  {
    label: 'PT Serif',
    value: 'PT+Serif'
  },
  {
    label: 'Open Sans Condensed',
    value: 'Open+Sans+Condensed'
  },
  {
    label: 'Lora',
    value: 'Lora'
  },
  {
    label: 'Slabo 27px',
    value: 'Slabo+27px'
  },
  {
    label: 'Nunito',
    value: 'Nunito'
  },
  {
    label: 'Noto Sans JP',
    value: 'Noto+Sans+JP'
  },
  {
    label: 'Rubik',
    value: 'Rubik'
  },
  {
    label: 'Titillium Web',
    value: 'Titillium+Web'
  },
  {
    label: 'Noto Serif',
    value: 'Noto+Serif'
  },
  {
    label: 'Fira Sans',
    value: 'Fira+Sans'
  },
  {
    label: 'Work Sans',
    value: 'Work+Sans'
  },
  {
    label: 'Quicksand',
    value: 'Quicksand'
  },
  {
    label: 'Noto Sans KR',
    value: 'Noto+Sans+KR'
  },
  {
    label: 'Nanum Gothic',
    value: 'Nanum+Gothic'
  },
  {
    label: 'PT Sans Narrow',
    value: 'PT+Sans+Narrow'
  },
  {
    label: 'Mukta',
    value: 'Mukta'
  },
  {
    label: 'Heebo',
    value: 'Heebo'
  },
  {
    label: 'Nunito Sans',
    value: 'Nunito+Sans'
  },
  {
    label: 'Arimo',
    value: 'Arimo'
  },
  {
    label: 'Inconsolata',
    value: 'Inconsolata'
  },
  {
    label: 'Noto Sans TC',
    value: 'Noto+Sans+TC'
  },
  {
    label: 'Dosis',
    value: 'Dosis'
  },
  {
    label: 'Karla',
    value: 'Karla'
  },
  {
    label: 'Oxygen',
    value: 'Oxygen'
  },
  {
    label: 'Barlow',
    value: 'Barlow'
  },
  {
    label: 'Libre Franklin',
    value: 'Libre+Franklin'
  },
  {
    label: 'Libre Baskerville',
    value: 'Libre+Baskerville'
  },
  {
    label: 'Crimson Text',
    value: 'Crimson+Text'
  },
  {
    label: 'Bitter',
    value: 'Bitter'
  },
  {
    label: 'Amiri',
    value: 'Amiri'
  },
  {
    label: 'Hind Siliguri',
    value: 'Hind+Siliguri'
  },
  {
    label: 'Cabin',
    value: 'Cabin'
  },
  {
    label: 'Hind',
    value: 'Hind'
  },
  {
    label: 'Source Code Pro',
    value: 'Source+Code+Pro'
  },
  {
    label: 'Josefin Sans',
    value: 'Josefin+Sans'
  },
  {
    label: 'Anton',
    value: 'Anton'
  },
  {
    label: 'Abel',
    value: 'Abel'
  },
  {
    label: 'Source Serif Pro',
    value: 'Source+Serif+Pro'
  },
  {
    label: 'Fjalla One',
    value: 'Fjalla+One'
  },
  {
    label: 'Indie Flower',
    value: 'Indie+Flower'
  },
  {
    label: 'Lobster',
    value: 'Lobster'
  },
  {
    label: 'Merriweather Sans',
    value: 'Merriweather+Sans'
  },
  {
    label: 'Barlow Condensed',
    value: 'Barlow+Condensed'
  },
  {
    label: 'Yanone Kaffeesatz',
    value: 'Yanone+Kaffeesatz'
  },
  {
    label: 'Pacifico',
    value: 'Pacifico'
  },
  {
    label: 'Dancing Script',
    value: 'Dancing+Script'
  },
  {
    label: 'Arvo',
    value: 'Arvo'
  },
  {
    label: 'Exo 2',
    value: 'Exo+2'
  },
  {
    label: 'Varela Round',
    value: 'Varela+Round'
  },
  {
    label: 'Cairo',
    value: 'Cairo'
  },
  {
    label: 'Questrial',
    value: 'Questrial'
  },
  {
    label: 'Kanit',
    value: 'Kanit'
  },
  {
    label: 'IBM Plex Sans',
    value: 'IBM+Plex+Sans'
  },
  {
    label: 'Overpass',
    value: 'Overpass'
  },
  {
    label: 'Shadows Into Light',
    value: 'Shadows+Into+Light'
  },
  {
    label: 'Prompt',
    value: 'Prompt'
  },
  {
    label: 'Teko',
    value: 'Teko'
  },
  {
    label: 'Martel',
    value: 'Martel'
  },
  {
    label: 'Hind Madurai',
    value: 'Hind+Madurai'
  },
  {
    label: 'Comfortaa',
    value: 'Comfortaa'
  },
  {
    label: 'Abril Fatface',
    value: 'Abril+Fatface'
  },
  {
    label: 'Catamaran',
    value: 'Catamaran'
  },
  {
    label: 'Asap',
    value: 'Asap'
  },
  {
    label: 'Acme',
    value: 'Acme'
  },
  {
    label: 'Archivo Narrow',
    value: 'Archivo+Narrow'
  },
  {
    label: 'Zilla Slab',
    value: 'Zilla+Slab'
  },
  {
    label: 'Exo',
    value: 'Exo'
  },
  {
    label: 'Amatic SC',
    value: 'Amatic+SC'
  },
  {
    label: 'Bree Serif',
    value: 'Bree+Serif'
  },
  {
    label: 'Noto Sans SC',
    value: 'Noto+Sans+SC'
  },
  {
    label: 'Righteous',
    value: 'Righteous'
  },
  {
    label: 'EB Garamond',
    value: 'EB+Garamond'
  },
  {
    label: 'Fira Sans Condensed',
    value: 'Fira+Sans+Condensed'
  },
  {
    label: 'Signika',
    value: 'Signika'
  },
  {
    label: 'Play',
    value: 'Play'
  },
  {
    label: 'Rajdhani',
    value: 'Rajdhani'
  },
  {
    label: 'IBM Plex Serif',
    value: 'IBM+Plex+Serif'
  },
  {
    label: 'Maven Pro',
    value: 'Maven+Pro'
  },
  {
    label: 'Cormorant Garamond',
    value: 'Cormorant+Garamond'
  },
  {
    label: 'Fredoka One',
    value: 'Fredoka+One'
  },
  {
    label: 'Patua One',
    value: 'Patua+One'
  },
  {
    label: 'Vollkorn',
    value: 'Vollkorn'
  },
  {
    label: 'Assistant',
    value: 'Assistant'
  },
  {
    label: 'PT Sans Caption',
    value: 'PT+Sans+Caption'
  },
  {
    label: 'Ubuntu Condensed',
    value: 'Ubuntu+Condensed'
  },
  {
    label: 'Bebas Neue',
    value: 'Bebas+Neue'
  },
  {
    label: 'Domine',
    value: 'Domine'
  },
  {
    label: 'Caveat',
    value: 'Caveat'
  },
  {
    label: 'Permanent Marker',
    value: 'Permanent+Marker'
  },
  {
    label: 'Ropa Sans',
    value: 'Ropa+Sans'
  },
  {
    label: 'Cinzel',
    value: 'Cinzel'
  },
  {
    label: 'Francois One',
    value: 'Francois+One'
  },
  {
    label: 'Patrick Hand',
    value: 'Patrick+Hand'
  },
  {
    label: 'ABeeZee',
    value: 'ABeeZee'
  },
  {
    label: 'Crete Round',
    value: 'Crete+Round'
  },
  {
    label: 'Alegreya Sans',
    value: 'Alegreya+Sans'
  },
  {
    label: 'Satisfy',
    value: 'Satisfy'
  },
  {
    label: 'Tajawal',
    value: 'Tajawal'
  },
  {
    label: 'Rokkitt',
    value: 'Rokkitt'
  },
  {
    label: 'Cuprum',
    value: 'Cuprum'
  },
  {
    label: 'Passion One',
    value: 'Passion+One'
  },
  {
    label: 'Barlow Semi Condensed',
    value: 'Barlow+Semi+Condensed'
  },
  {
    label: 'Tinos',
    value: 'Tinos'
  },
  {
    label: 'Noticia Text',
    value: 'Noticia+Text'
  },
  {
    label: 'Courgette',
    value: 'Courgette'
  },
  {
    label: 'Alfa Slab One',
    value: 'Alfa+Slab+One'
  },
  {
    label: 'Nanum Myeongjo',
    value: 'Nanum+Myeongjo'
  },
  {
    label: 'Alegreya',
    value: 'Alegreya'
  },
  {
    label: 'Special Elite',
    value: 'Special+Elite'
  },
  {
    label: 'Frank Ruhl Libre',
    value: 'Frank+Ruhl+Libre'
  },
  {
    label: 'Viga',
    value: 'Viga'
  },
  {
    label: 'Kalam',
    value: 'Kalam'
  },
  {
    label: 'Noto Serif JP',
    value: 'Noto+Serif+JP'
  },
  {
    label: 'Kaushan Script',
    value: 'Kaushan+Script'
  },
  {
    label: 'Monda',
    value: 'Monda'
  },
  {
    label: 'Volkhov',
    value: 'Volkhov'
  },
  {
    label: 'Merienda',
    value: 'Merienda'
  },
  {
    label: 'Cardo',
    value: 'Cardo'
  },
  {
    label: 'Great Vibes',
    value: 'Great+Vibes'
  },
  {
    label: 'Hind Vadodara',
    value: 'Hind+Vadodara'
  },
  {
    label: 'Lobster Two',
    value: 'Lobster+Two'
  },
  {
    label: 'News Cycle',
    value: 'News+Cycle'
  },
  {
    label: 'Montserrat Alternates',
    value: 'Montserrat+Alternates'
  },
  {
    label: 'Pathway Gothic One',
    value: 'Pathway+Gothic+One'
  },
  {
    label: 'Quattrocento Sans',
    value: 'Quattrocento+Sans'
  },
  {
    label: 'Archivo Black',
    value: 'Archivo+Black'
  },
  {
    label: 'DM Sans',
    value: 'DM+Sans'
  },
  {
    label: 'Gochi Hand',
    value: 'Gochi+Hand'
  },
  {
    label: 'Sarabun',
    value: 'Sarabun'
  },
  {
    label: 'Sacramento',
    value: 'Sacramento'
  },
  {
    label: 'Fira Sans Extra Condensed',
    value: 'Fira+Sans+Extra+Condensed'
  },
  {
    label: 'Cantarell',
    value: 'Cantarell'
  },
  {
    label: 'Old Standard TT',
    value: 'Old+Standard+TT'
  },
  {
    label: 'Taviraj',
    value: 'Taviraj'
  },
  {
    label: 'Istok Web',
    value: 'Istok+Web'
  },
  {
    label: 'Changa',
    value: 'Changa'
  },
  {
    label: 'Asap Condensed',
    value: 'Asap+Condensed'
  },
  {
    label: 'Concert One',
    value: 'Concert+One'
  },
  {
    label: 'Yantramanav',
    value: 'Yantramanav'
  },
  {
    label: 'Archivo',
    value: 'Archivo'
  },
  {
    label: 'Orbitron',
    value: 'Orbitron'
  },
  {
    label: 'Didact Gothic',
    value: 'Didact+Gothic'
  },
  {
    label: 'Luckiest Guy',
    value: 'Luckiest+Guy'
  },
  {
    label: 'Parisienne',
    value: 'Parisienne'
  },
  {
    label: 'Playfair Display SC',
    value: 'Playfair+Display+SC'
  },
  {
    label: 'Handlee',
    value: 'Handlee'
  },
  {
    label: 'Chivo',
    value: 'Chivo'
  },
  {
    label: 'Martel Sans',
    value: 'Martel+Sans'
  },
  {
    label: 'Sanchez',
    value: 'Sanchez'
  },
  {
    label: 'Hind Guntur',
    value: 'Hind+Guntur'
  },
  {
    label: 'Gloria Hallelujah',
    value: 'Gloria+Hallelujah'
  },
  {
    label: 'Quattrocento',
    value: 'Quattrocento'
  },
  {
    label: 'PT Mono',
    value: 'PT+Mono'
  },
  {
    label: 'Economica',
    value: 'Economica'
  },
  {
    label: 'Prata',
    value: 'Prata'
  },
  {
    label: 'Cookie',
    value: 'Cookie'
  },
  {
    label: 'Poiret One',
    value: 'Poiret+One'
  },
  {
    label: 'M PLUS 1p',
    value: 'M+PLUS+1p'
  },
  {
    label: 'Advent Pro',
    value: 'Advent+Pro'
  },
  {
    label: 'Ruda',
    value: 'Ruda'
  },
  {
    label: 'Neuton',
    value: 'Neuton'
  },
  {
    label: 'Russo One',
    value: 'Russo+One'
  },
  {
    label: 'BenchNine',
    value: 'BenchNine'
  },
  {
    label: 'Do Hyeon',
    value: 'Do+Hyeon'
  },
  {
    label: 'Oleo Script',
    value: 'Oleo+Script'
  },
  {
    label: 'Lalezar',
    value: 'Lalezar'
  },
  {
    label: 'Monoton',
    value: 'Monoton'
  },
  {
    label: 'Pragati Narrow',
    value: 'Pragati+Narrow'
  },
  {
    label: 'Markazi Text',
    value: 'Markazi+Text'
  },
  {
    label: 'Gudea',
    value: 'Gudea'
  },
  {
    label: 'Mitr',
    value: 'Mitr'
  },
  {
    label: 'Sawarabi Mincho',
    value: 'Sawarabi+Mincho'
  },
  {
    label: 'Bangers',
    value: 'Bangers'
  },
  {
    label: 'Josefin Slab',
    value: 'Josefin+Slab'
  },
  {
    label: 'Vidaloka',
    value: 'Vidaloka'
  },
  {
    label: 'Philosopher',
    value: 'Philosopher'
  },
  {
    label: 'Gentium Basic',
    value: 'Gentium+Basic'
  },
  {
    label: 'Ultra',
    value: 'Ultra'
  },
  {
    label: 'Cambay',
    value: 'Cambay'
  },
  {
    label: 'Electrolize',
    value: 'Electrolize'
  },
  {
    label: 'Spectral',
    value: 'Spectral'
  },
  {
    label: 'El Messiri',
    value: 'El+Messiri'
  },
  {
    label: 'Titan One',
    value: 'Titan+One'
  },
  {
    label: 'Marck Script',
    value: 'Marck+Script'
  },
  {
    label: 'Pangolin',
    value: 'Pangolin'
  },
  {
    label: 'Neucha',
    value: 'Neucha'
  },
  {
    label: 'Press Start 2P',
    value: 'Press+Start+2P'
  },
  {
    label: 'Hammersmith One',
    value: 'Hammersmith+One'
  },
  {
    label: 'Alice',
    value: 'Alice'
  },
  {
    label: 'Kreon',
    value: 'Kreon'
  },
  {
    label: 'Boogaloo',
    value: 'Boogaloo'
  },
  {
    label: 'Sigmar One',
    value: 'Sigmar+One'
  },
  {
    label: 'Arapey',
    value: 'Arapey'
  },
  {
    label: 'Amaranth',
    value: 'Amaranth'
  },
  {
    label: 'Paytone One',
    value: 'Paytone+One'
  },
  {
    label: 'Adamina',
    value: 'Adamina'
  },
  {
    label: 'DM Serif Display',
    value: 'DM+Serif+Display'
  },
  {
    label: 'Alef',
    value: 'Alef'
  },
  {
    label: 'M PLUS Rounded 1c',
    value: 'M+PLUS+Rounded+1c'
  },
  {
    label: 'Actor',
    value: 'Actor'
  },
  {
    label: 'Rochester',
    value: 'Rochester'
  },
  {
    label: 'Aclonica',
    value: 'Aclonica'
  },
  {
    label: 'Architects Daughter',
    value: 'Architects+Daughter'
  },
  {
    label: 'Nanum Pen Script',
    value: 'Nanum+Pen+Script'
  },
  {
    label: 'Sarala',
    value: 'Sarala'
  },
  {
    label: 'Unica One',
    value: 'Unica+One'
  },
  {
    label: 'Nanum Gothic Coding',
    value: 'Nanum+Gothic+Coding'
  },
  {
    label: 'Yellowtail',
    value: 'Yellowtail'
  },
  {
    label: 'Audiowide',
    value: 'Audiowide'
  },
  {
    label: 'Saira Condensed',
    value: 'Saira+Condensed'
  },
  {
    label: 'Pontano Sans',
    value: 'Pontano+Sans'
  },
  {
    label: 'Staatliches',
    value: 'Staatliches'
  },
  {
    label: 'Cabin Condensed',
    value: 'Cabin+Condensed'
  },
  {
    label: 'Enriqueta',
    value: 'Enriqueta'
  },
  {
    label: 'Yeseva One',
    value: 'Yeseva+One'
  },
  {
    label: 'Rock Salt',
    value: 'Rock+Salt'
  },
  {
    label: 'Julius Sans One',
    value: 'Julius+Sans+One'
  },
  {
    label: 'Gothic A1',
    value: 'Gothic+A1'
  },
  {
    label: 'Shadows Into Light Two',
    value: 'Shadows+Into+Light+Two'
  },
  {
    label: 'Signika Negative',
    value: 'Signika+Negative'
  },
  {
    label: 'Tangerine',
    value: 'Tangerine'
  },
  {
    label: 'Homemade Apple',
    value: 'Homemade+Apple'
  },
  {
    label: 'Gentium Book Basic',
    value: 'Gentium+Book+Basic'
  },
  {
    label: 'Spicy Rice',
    value: 'Spicy+Rice'
  },
  {
    label: 'Saira',
    value: 'Saira'
  },
  {
    label: 'Bad Script',
    value: 'Bad+Script'
  },
  {
    label: 'Bowlby One SC',
    value: 'Bowlby+One+SC'
  },
  {
    label: 'Khand',
    value: 'Khand'
  },
  {
    label: 'Scada',
    value: 'Scada'
  },
  {
    label: 'Karma',
    value: 'Karma'
  },
  {
    label: 'Fugaz One',
    value: 'Fugaz+One'
  },
  {
    label: 'Rambla',
    value: 'Rambla'
  },
  {
    label: 'Squada One',
    value: 'Squada+One'
  },
  {
    label: 'Basic',
    value: 'Basic'
  },
  {
    label: 'Pridi',
    value: 'Pridi'
  },
  {
    label: 'Playball',
    value: 'Playball'
  },
  {
    label: 'Yrsa',
    value: 'Yrsa'
  },
  {
    label: 'Ramabhadra',
    value: 'Ramabhadra'
  },
  {
    label: 'Abhaya Libre',
    value: 'Abhaya+Libre'
  },
  {
    label: 'Encode Sans Condensed',
    value: 'Encode+Sans+Condensed'
  },
  {
    label: 'Carter One',
    value: 'Carter+One'
  },
  {
    label: 'Allura',
    value: 'Allura'
  },
  {
    label: 'Armata',
    value: 'Armata'
  },
  {
    label: 'Covered By Your Grace',
    value: 'Covered+By+Your+Grace'
  },
  {
    label: 'Quantico',
    value: 'Quantico'
  },
  {
    label: 'Cormorant',
    value: 'Cormorant'
  },
  {
    label: 'Coda',
    value: 'Coda'
  },
  {
    label: 'Raleway Dots',
    value: 'Raleway+Dots'
  },
  {
    label: 'Lusitana',
    value: 'Lusitana'
  },
  {
    label: 'Sorts Mill Goudy',
    value: 'Sorts+Mill+Goudy'
  },
  {
    label: 'Khula',
    value: 'Khula'
  },
  {
    label: 'Reenie Beanie',
    value: 'Reenie+Beanie'
  },
  {
    label: 'Lilita One',
    value: 'Lilita+One'
  },
  {
    label: 'Cantata One',
    value: 'Cantata+One'
  },
  {
    label: 'Average',
    value: 'Average'
  },
  {
    label: 'PT Serif Caption',
    value: 'PT+Serif+Caption'
  },
  {
    label: 'Damion',
    value: 'Damion'
  },
  {
    label: 'Sintony',
    value: 'Sintony'
  },
  {
    label: 'Arsenal',
    value: 'Arsenal'
  },
  {
    label: 'Ubuntu Mono',
    value: 'Ubuntu+Mono'
  },
  {
    label: 'Varela',
    value: 'Varela'
  },
  {
    label: 'Chewy',
    value: 'Chewy'
  },
  {
    label: 'Unna',
    value: 'Unna'
  },
  {
    label: 'DM Serif Text',
    value: 'DM+Serif+Text'
  },
  {
    label: 'Black Ops One',
    value: 'Black+Ops+One'
  },
  {
    label: 'ZCOOL XiaoWei',
    value: 'ZCOOL+XiaoWei'
  },
  {
    label: 'Jura',
    value: 'Jura'
  },
  {
    label: 'Fauna One',
    value: 'Fauna+One'
  },
  {
    label: 'Marcellus',
    value: 'Marcellus'
  },
  {
    label: 'Sawarabi Gothic',
    value: 'Sawarabi+Gothic'
  },
  {
    label: 'Encode Sans',
    value: 'Encode+Sans'
  },
  {
    label: 'Rubik Mono One',
    value: 'Rubik+Mono+One'
  },
  {
    label: 'Pinyon Script',
    value: 'Pinyon+Script'
  },
  {
    label: 'Tenor Sans',
    value: 'Tenor+Sans'
  },
  {
    label: 'Lateef',
    value: 'Lateef'
  },
  {
    label: 'Alegreya Sans SC',
    value: 'Alegreya+Sans+SC'
  },
  {
    label: 'IBM Plex Mono',
    value: 'IBM+Plex+Mono'
  },
  {
    label: 'Jaldi',
    value: 'Jaldi'
  },
  {
    label: 'Palanquin',
    value: 'Palanquin'
  },
  {
    label: 'Noto Serif SC',
    value: 'Noto+Serif+SC'
  },
  {
    label: 'Mr Dafoe',
    value: 'Mr+Dafoe'
  },
  {
    label: 'Share',
    value: 'Share'
  },
  {
    label: 'Caveat Brush',
    value: 'Caveat+Brush'
  },
  {
    label: 'Michroma',
    value: 'Michroma'
  },
  {
    label: 'Alex Brush',
    value: 'Alex+Brush'
  },
  {
    label: 'Chakra Petch',
    value: 'Chakra+Petch'
  },
  {
    label: 'Antic Slab',
    value: 'Antic+Slab'
  },
  {
    label: 'Hanuman',
    value: 'Hanuman'
  },
  {
    label: 'Allan',
    value: 'Allan'
  },
  {
    label: 'Spinnaker',
    value: 'Spinnaker'
  },
  {
    label: 'Itim',
    value: 'Itim'
  },
  {
    label: 'Mukta Malar',
    value: 'Mukta+Malar'
  },
  {
    label: 'Nothing You Could Do',
    value: 'Nothing+You+Could+Do'
  },
  {
    label: 'Glegoo',
    value: 'Glegoo'
  },
  {
    label: 'Krub',
    value: 'Krub'
  },
  {
    label: 'Rancho',
    value: 'Rancho'
  },
  {
    label: 'Annie Use Your Telescope',
    value: 'Annie+Use+Your+Telescope'
  },
  {
    label: 'Kelly Slab',
    value: 'Kelly+Slab'
  },
  {
    label: 'Overlock',
    value: 'Overlock'
  },
  {
    label: 'Antic',
    value: 'Antic'
  },
  {
    label: 'Anonymous Pro',
    value: 'Anonymous+Pro'
  },
  {
    label: 'Cousine',
    value: 'Cousine'
  },
  {
    label: 'Trirong',
    value: 'Trirong'
  },
  {
    label: 'Fira Mono',
    value: 'Fira+Mono'
  },
  {
    label: 'Arima Madurai',
    value: 'Arima+Madurai'
  },
  {
    label: 'Reem Kufi',
    value: 'Reem+Kufi'
  },
  {
    label: 'Leckerli One',
    value: 'Leckerli+One'
  },
  {
    label: 'Miriam Libre',
    value: 'Miriam+Libre'
  },
  {
    label: 'Black Han Sans',
    value: 'Black+Han+Sans'
  },
  {
    label: 'Scheherazade',
    value: 'Scheherazade'
  },
  {
    label: 'Mali',
    value: 'Mali'
  },
  {
    label: 'Eczar',
    value: 'Eczar'
  },
  {
    label: 'Rufina',
    value: 'Rufina'
  },
  {
    label: 'Just Another Hand',
    value: 'Just+Another+Hand'
  },
  {
    label: 'Forum',
    value: 'Forum'
  },
  {
    label: 'Cabin Sketch',
    value: 'Cabin+Sketch'
  },
  {
    label: 'Candal',
    value: 'Candal'
  },
  {
    label: 'Sriracha',
    value: 'Sriracha'
  },
  {
    label: 'Fredericka the Great',
    value: 'Fredericka+the+Great'
  },
  {
    label: 'Mallanna',
    value: 'Mallanna'
  },
  {
    label: 'Days One',
    value: 'Days+One'
  },
  {
    label: 'Bevan',
    value: 'Bevan'
  },
  {
    label: 'Bai Jamjuree',
    value: 'Bai+Jamjuree'
  },
  {
    label: 'Cinzel Decorative',
    value: 'Cinzel+Decorative'
  },
  {
    label: 'Aldrich',
    value: 'Aldrich'
  },
  {
    label: 'Shrikhand',
    value: 'Shrikhand'
  },
  {
    label: 'Knewave',
    value: 'Knewave'
  },
  {
    label: 'Berkshire Swash',
    value: 'Berkshire+Swash'
  },
  {
    label: 'VT323',
    value: 'VT323'
  },
  {
    label: 'Overpass Mono',
    value: 'Overpass+Mono'
  },
  {
    label: 'Six Caps',
    value: 'Six+Caps'
  },
  {
    label: 'Suez One',
    value: 'Suez+One'
  },
  {
    label: 'Space Mono',
    value: 'Space+Mono'
  },
  {
    label: 'Pattaya',
    value: 'Pattaya'
  },
  {
    label: 'Coming Soon',
    value: 'Coming+Soon'
  },
  {
    label: 'Arbutus Slab',
    value: 'Arbutus+Slab'
  },
  {
    label: 'Share Tech Mono',
    value: 'Share+Tech+Mono'
  },
  {
    label: 'Rasa',
    value: 'Rasa'
  },
  {
    label: 'Coustard',
    value: 'Coustard'
  },
  {
    label: 'Aleo',
    value: 'Aleo'
  },
  {
    label: 'Oranienbaum',
    value: 'Oranienbaum'
  },
  {
    label: 'Bungee',
    value: 'Bungee'
  },
  {
    label: 'Saira Semi Condensed',
    value: 'Saira+Semi+Condensed'
  },
  {
    label: 'Allerta Stencil',
    value: 'Allerta+Stencil'
  },
  {
    label: 'Marmelad',
    value: 'Marmelad'
  },
  {
    label: 'Italianno',
    value: 'Italianno'
  },
  {
    label: 'Almarai',
    value: 'Almarai'
  },
  {
    label: 'Caudex',
    value: 'Caudex'
  },
  {
    label: 'Halant',
    value: 'Halant'
  },
  {
    label: 'Nobile',
    value: 'Nobile'
  },
  {
    label: 'Judson',
    value: 'Judson'
  },
  {
    label: 'Marcellus SC',
    value: 'Marcellus+SC'
  },
  {
    label: 'Biryani',
    value: 'Biryani'
  },
  {
    label: 'Allerta',
    value: 'Allerta'
  },
  {
    label: 'Magra',
    value: 'Magra'
  },
  {
    label: 'Saira Extra Condensed',
    value: 'Saira+Extra+Condensed'
  },
  {
    label: 'Niconne',
    value: 'Niconne'
  },
  {
    label: 'Mada',
    value: 'Mada'
  },
  {
    label: 'Sniglet',
    value: 'Sniglet'
  },
  {
    label: 'Gruppo',
    value: 'Gruppo'
  },
  {
    label: 'Average Sans',
    value: 'Average+Sans'
  },
  {
    label: 'Londrina Solid',
    value: 'Londrina+Solid'
  },
  {
    label: 'Grand Hotel',
    value: 'Grand+Hotel'
  },
  {
    label: 'Contrail One',
    value: 'Contrail+One'
  },
  {
    label: 'Yesteryear',
    value: 'Yesteryear'
  },
  {
    label: 'Mrs Saint Delafield',
    value: 'Mrs+Saint+Delafield'
  },
  {
    label: 'Noto Serif TC',
    value: 'Noto+Serif+TC'
  },
  {
    label: 'Bungee Inline',
    value: 'Bungee+Inline'
  },
  {
    label: 'Coda Caption',
    value: 'Coda+Caption'
  },
  {
    label: 'Kameron',
    value: 'Kameron'
  },
  {
    label: 'Lemonada',
    value: 'Lemonada'
  },
  {
    label: 'Nanum Brush Script',
    value: 'Nanum+Brush+Script'
  },
  {
    label: 'Frijole',
    value: 'Frijole'
  },
  {
    label: 'Norican',
    value: 'Norican'
  },
  {
    label: 'Racing Sans One',
    value: 'Racing+Sans+One'
  },
  {
    label: 'Syncopate',
    value: 'Syncopate'
  },
  {
    label: 'Kadwa',
    value: 'Kadwa'
  },
  {
    label: 'Lexend Deca',
    value: 'Lexend+Deca'
  },
  {
    label: 'Sofia',
    value: 'Sofia'
  },
  {
    label: 'Capriola',
    value: 'Capriola'
  },
  {
    label: 'Sunflower',
    value: 'Sunflower'
  },
  {
    label: 'Copse',
    value: 'Copse'
  },
  {
    label: 'Molengo',
    value: 'Molengo'
  },
  {
    label: 'Bentham',
    value: 'Bentham'
  },
  {
    label: 'Kosugi Maru',
    value: 'Kosugi+Maru'
  },
  {
    label: 'Aladin',
    value: 'Aladin'
  },
  {
    label: 'Telex',
    value: 'Telex'
  },
  {
    label: 'Alegreya SC',
    value: 'Alegreya+SC'
  },
  {
    label: 'Buenard',
    value: 'Buenard'
  },
  {
    label: 'Cedarville Cursive',
    value: 'Cedarville+Cursive'
  },
  {
    label: 'Amethysta',
    value: 'Amethysta'
  },
  {
    label: 'IBM Plex Sans Condensed',
    value: 'IBM+Plex+Sans+Condensed'
  },
  {
    label: 'Maitree',
    value: 'Maitree'
  },
  {
    label: 'Sen',
    value: 'Sen'
  },
  {
    label: 'Arizonia',
    value: 'Arizonia'
  },
  {
    label: 'IM Fell Double Pica',
    value: 'IM+Fell+Double+Pica'
  },
  {
    label: 'Schoolbell',
    value: 'Schoolbell'
  },
  {
    label: 'Red Hat Display',
    value: 'Red+Hat+Display'
  },
  {
    label: 'Lemon',
    value: 'Lemon'
  },
  {
    label: 'Noto Serif KR',
    value: 'Noto+Serif+KR'
  },
  {
    label: 'Chonburi',
    value: 'Chonburi'
  },
  {
    label: 'Mukta Vaani',
    value: 'Mukta+Vaani'
  },
  {
    label: 'Bubblegum Sans',
    value: 'Bubblegum+Sans'
  },
  {
    label: 'Battambang',
    value: 'Battambang'
  },
  {
    label: 'Krona One',
    value: 'Krona+One'
  },
  {
    label: 'Changa One',
    value: 'Changa+One'
  },
  {
    label: 'Lustria',
    value: 'Lustria'
  },
  {
    label: 'Graduate',
    value: 'Graduate'
  },
  {
    label: 'Megrim',
    value: 'Megrim'
  },
  {
    label: 'Merienda One',
    value: 'Merienda+One'
  },
  {
    label: 'Carrois Gothic',
    value: 'Carrois+Gothic'
  },
  {
    label: 'Radley',
    value: 'Radley'
  },
  {
    label: 'Marvel',
    value: 'Marvel'
  },
  {
    label: 'Rosario',
    value: 'Rosario'
  },
  {
    label: 'Carme',
    value: 'Carme'
  },
  {
    label: 'Rozha One',
    value: 'Rozha+One'
  },
  {
    label: 'Cutive Mono',
    value: 'Cutive+Mono'
  },
  {
    label: 'Jockey One',
    value: 'Jockey+One'
  },
  {
    label: 'Noto Sans HK',
    value: 'Noto+Sans+HK'
  },
  {
    label: 'Petit Formal Script',
    value: 'Petit+Formal+Script'
  },
  {
    label: 'Voltaire',
    value: 'Voltaire'
  },
  {
    label: 'Love Ya Like A Sister',
    value: 'Love+Ya+Like+A+Sister'
  },
  {
    label: 'Delius',
    value: 'Delius'
  },
  {
    label: 'Shojumaru',
    value: 'Shojumaru'
  },
  {
    label: 'Chelsea Market',
    value: 'Chelsea+Market'
  },
  {
    label: 'Suranna',
    value: 'Suranna'
  },
  {
    label: 'Nixie One',
    value: 'Nixie+One'
  },
  {
    label: 'Ovo',
    value: 'Ovo'
  },
  {
    label: 'Alike Angular',
    value: 'Alike+Angular'
  },
  {
    label: 'Oxygen Mono',
    value: 'Oxygen+Mono'
  },
  {
    label: 'Rye',
    value: 'Rye'
  },
  {
    label: 'Metrophobic',
    value: 'Metrophobic'
  },
  {
    label: 'Public Sans',
    value: 'Public+Sans'
  },
  {
    label: 'Sansita',
    value: 'Sansita'
  },
  {
    label: 'Lekton',
    value: 'Lekton'
  },
  {
    label: 'Slabo 13px',
    value: 'Slabo+13px'
  },
  {
    label: 'Emilys Candy',
    value: 'Emilys+Candy'
  },
  {
    label: 'Ceviche One',
    value: 'Ceviche+One'
  },
  {
    label: 'Calligraffitti',
    value: 'Calligraffitti'
  },
  {
    label: 'Duru Sans',
    value: 'Duru+Sans'
  },
  {
    label: 'Faustina',
    value: 'Faustina'
  },
  {
    label: 'Kristi',
    value: 'Kristi'
  },
  {
    label: 'Cormorant Infant',
    value: 'Cormorant+Infant'
  },
  {
    label: 'Gilda Display',
    value: 'Gilda+Display'
  },
  {
    label: 'Trocchi',
    value: 'Trocchi'
  },
  {
    label: 'Averia Serif Libre',
    value: 'Averia+Serif+Libre'
  },
  {
    label: 'Cutive',
    value: 'Cutive'
  },
  {
    label: 'Sue Ellen Francisco',
    value: 'Sue+Ellen+Francisco'
  },
  {
    label: 'Mate',
    value: 'Mate'
  },
  {
    label: 'Palanquin Dark',
    value: 'Palanquin+Dark'
  },
  {
    label: 'Liu Jian Mao Cao',
    value: 'Liu+Jian+Mao+Cao'
  },
  {
    label: 'Inter',
    value: 'Inter'
  },
  {
    label: 'Freckle Face',
    value: 'Freckle+Face'
  },
  {
    label: 'Goudy Bookletter 1911',
    value: 'Goudy+Bookletter+1911'
  },
  {
    label: 'GFS Didot',
    value: 'GFS+Didot'
  },
  {
    label: 'McLaren',
    value: 'McLaren'
  },
  {
    label: 'Fresca',
    value: 'Fresca'
  },
  {
    label: 'Gurajada',
    value: 'Gurajada'
  },
  {
    label: 'Herr Von Muellerhoff',
    value: 'Herr+Von+Muellerhoff'
  },
  {
    label: 'Literata',
    value: 'Literata'
  },
  {
    label: 'Amiko',
    value: 'Amiko'
  },
  {
    label: 'Andada',
    value: 'Andada'
  },
  {
    label: 'Secular One',
    value: 'Secular+One'
  },
  {
    label: 'Montez',
    value: 'Montez'
  },
  {
    label: 'Seaweed Script',
    value: 'Seaweed+Script'
  },
  {
    label: 'Niramit',
    value: 'Niramit'
  },
  {
    label: 'Poly',
    value: 'Poly'
  },
  {
    label: 'Gabriela',
    value: 'Gabriela'
  },
  {
    label: 'Pompiere',
    value: 'Pompiere'
  },
  {
    label: 'Federo',
    value: 'Federo'
  },
  {
    label: 'Vesper Libre',
    value: 'Vesper+Libre'
  },
  {
    label: 'Mirza',
    value: 'Mirza'
  },
  {
    label: 'UnifrakturMaguntia',
    value: 'UnifrakturMaguntia'
  },
  {
    label: 'Sedgwick Ave',
    value: 'Sedgwick+Ave'
  },
  {
    label: 'Cambo',
    value: 'Cambo'
  },
  {
    label: 'Mr De Haviland',
    value: 'Mr+De+Haviland'
  },
  {
    label: 'Harmattan',
    value: 'Harmattan'
  },
  {
    label: 'Athiti',
    value: 'Athiti'
  },
  {
    label: 'Waiting for the Sunrise',
    value: 'Waiting+for+the+Sunrise'
  },
  {
    label: 'Esteban',
    value: 'Esteban'
  },
  {
    label: 'IM Fell English',
    value: 'IM+Fell+English'
  },
  {
    label: 'Encode Sans Semi Expanded',
    value: 'Encode+Sans+Semi+Expanded'
  },
  {
    label: 'Belleza',
    value: 'Belleza'
  },
  {
    label: 'K2D',
    value: 'K2D'
  },
  {
    label: 'Fondamento',
    value: 'Fondamento'
  },
  {
    label: 'Fanwood Text',
    value: 'Fanwood+Text'
  },
  {
    label: 'Doppio One',
    value: 'Doppio+One'
  },
  {
    label: 'Vast Shadow',
    value: 'Vast+Shadow'
  },
  {
    label: 'Jua',
    value: 'Jua'
  },
  {
    label: 'Rammetto One',
    value: 'Rammetto+One'
  },
  {
    label: 'Inder',
    value: 'Inder'
  },
  {
    label: 'Alike',
    value: 'Alike'
  },
  {
    label: 'Balthazar',
    value: 'Balthazar'
  },
  {
    label: 'Baloo Chettan 2',
    value: 'Baloo+Chettan+2'
  },
  {
    label: 'Amita',
    value: 'Amita'
  },
  {
    label: 'Gugi',
    value: 'Gugi'
  },
  {
    label: 'Proza Libre',
    value: 'Proza+Libre'
  },
  {
    label: 'Unkempt',
    value: 'Unkempt'
  },
  {
    label: 'NTR',
    value: 'NTR'
  },
  {
    label: 'Convergence',
    value: 'Convergence'
  },
  {
    label: 'Baumans',
    value: 'Baumans'
  },
  {
    label: 'Anaheim',
    value: 'Anaheim'
  },
  {
    label: 'Belgrano',
    value: 'Belgrano'
  },
  {
    label: 'Faster One',
    value: 'Faster+One'
  },
  {
    label: 'Bowlby One',
    value: 'Bowlby+One'
  },
  {
    label: 'Brawler',
    value: 'Brawler'
  },
  {
    label: 'Mountains of Christmas',
    value: 'Mountains+of+Christmas'
  },
  {
    label: 'Gelasio',
    value: 'Gelasio'
  },
  {
    label: 'Corben',
    value: 'Corben'
  },
  {
    label: 'IM Fell DW Pica',
    value: 'IM+Fell+DW+Pica'
  },
  {
    label: 'Crafty Girls',
    value: 'Crafty+Girls'
  },
  {
    label: 'Iceland',
    value: 'Iceland'
  },
  {
    label: 'IM Fell French Canon SC',
    value: 'IM+Fell+French+Canon+SC'
  },
  {
    label: 'Wallpoet',
    value: 'Wallpoet'
  },
  {
    label: 'Gravitas One',
    value: 'Gravitas+One'
  },
  {
    label: 'Red Hat Text',
    value: 'Red+Hat+Text'
  },
  {
    label: 'Galada',
    value: 'Galada'
  },
  {
    label: 'Padauk',
    value: 'Padauk'
  },
  {
    label: 'Kurale',
    value: 'Kurale'
  },
  {
    label: 'La Belle Aurore',
    value: 'La+Belle+Aurore'
  },
  {
    label: 'Montserrat Subrayada',
    value: 'Montserrat+Subrayada'
  },
  {
    label: 'Trade Winds',
    value: 'Trade+Winds'
  },
  {
    label: 'Cormorant SC',
    value: 'Cormorant+SC'
  },
  {
    label: 'Homenaje',
    value: 'Homenaje'
  },
  {
    label: 'Limelight',
    value: 'Limelight'
  },
  {
    label: 'Oleo Script Swash Caps',
    value: 'Oleo+Script+Swash+Caps'
  },
  {
    label: 'Voces',
    value: 'Voces'
  },
  {
    label: 'B612 Mono',
    value: 'B612+Mono'
  },
  {
    label: 'Stardos Stencil',
    value: 'Stardos+Stencil'
  },
  {
    label: 'Walter Turncoat',
    value: 'Walter+Turncoat'
  },
  {
    label: 'Mouse Memoirs',
    value: 'Mouse+Memoirs'
  },
  {
    label: 'Happy Monkey',
    value: 'Happy+Monkey'
  },
  {
    label: 'Quando',
    value: 'Quando'
  },
  {
    label: 'Encode Sans Semi Condensed',
    value: 'Encode+Sans+Semi+Condensed'
  },
  {
    label: 'Share Tech',
    value: 'Share+Tech'
  },
  {
    label: 'Short Stack',
    value: 'Short+Stack'
  },
  {
    label: 'Rouge Script',
    value: 'Rouge+Script'
  },
  {
    label: 'Headland One',
    value: 'Headland+One'
  },
  {
    label: 'Bungee Shade',
    value: 'Bungee+Shade'
  },
  {
    label: 'Zeyada',
    value: 'Zeyada'
  },
  {
    label: 'Numans',
    value: 'Numans'
  },
  {
    label: 'Denk One',
    value: 'Denk+One'
  },
  {
    label: 'Eater',
    value: 'Eater'
  },
  {
    label: 'Qwigley',
    value: 'Qwigley'
  },
  {
    label: 'Oregano',
    value: 'Oregano'
  },
  {
    label: 'Spectral SC',
    value: 'Spectral+SC'
  },
  {
    label: 'Patrick Hand SC',
    value: 'Patrick+Hand+SC'
  },
  {
    label: 'Andika',
    value: 'Andika'
  },
  {
    label: 'Strait',
    value: 'Strait'
  },
  {
    label: 'Livvic',
    value: 'Livvic'
  },
  {
    label: 'Expletus Sans',
    value: 'Expletus+Sans'
  },
  {
    label: 'Skranji',
    value: 'Skranji'
  },
  {
    label: 'Give You Glory',
    value: 'Give+You+Glory'
  },
  {
    label: 'Holtwood One SC',
    value: 'Holtwood+One+SC'
  },
  {
    label: 'Katibeh',
    value: 'Katibeh'
  },
  {
    label: 'Clicker Script',
    value: 'Clicker+Script'
  },
  {
    label: 'Dawning of a New Day',
    value: 'Dawning+of+a+New+Day'
  },
  {
    label: 'Spirax',
    value: 'Spirax'
  },
  {
    label: 'Aref Ruqaa',
    value: 'Aref+Ruqaa'
  },
  {
    label: 'Cherry Swash',
    value: 'Cherry+Swash'
  },
  {
    label: 'Puritan',
    value: 'Puritan'
  },
  {
    label: 'Carrois Gothic SC',
    value: 'Carrois+Gothic+SC'
  },
  {
    label: 'Rakkas',
    value: 'Rakkas'
  },
  {
    label: 'Baloo 2',
    value: 'Baloo+2'
  },
  {
    label: 'Averia Sans Libre',
    value: 'Averia+Sans+Libre'
  },
  {
    label: 'Podkova',
    value: 'Podkova'
  },
  {
    label: 'Cormorant Upright',
    value: 'Cormorant+Upright'
  },
  {
    label: 'Laila',
    value: 'Laila'
  },
  {
    label: 'Mako',
    value: 'Mako'
  },
  {
    label: 'BioRhyme',
    value: 'BioRhyme'
  },
  {
    label: 'Cantora One',
    value: 'Cantora+One'
  },
  {
    label: 'Atma',
    value: 'Atma'
  },
  {
    label: 'Fontdiner Swanky',
    value: 'Fontdiner+Swanky'
  },
  {
    label: 'Artifika',
    value: 'Artifika'
  },
  {
    label: 'Finger Paint',
    value: 'Finger+Paint'
  },
  {
    label: 'Nova Square',
    value: 'Nova+Square'
  },
  {
    label: 'Fjord One',
    value: 'Fjord+One'
  },
  {
    label: 'Tauri',
    value: 'Tauri'
  },
  {
    label: 'Macondo Swash Caps',
    value: 'Macondo+Swash+Caps'
  },
  {
    label: 'Jomolhari',
    value: 'Jomolhari'
  },
  {
    label: 'Bellefair',
    value: 'Bellefair'
  },
  {
    label: 'Wendy One',
    value: 'Wendy+One'
  },
  {
    label: 'Tienne',
    value: 'Tienne'
  },
  {
    label: 'Yatra One',
    value: 'Yatra+One'
  },
  {
    label: 'Delius Swash Caps',
    value: 'Delius+Swash+Caps'
  },
  {
    label: 'Life Savers',
    value: 'Life+Savers'
  },
  {
    label: 'Spartan',
    value: 'Spartan'
  },
  {
    label: 'Geo',
    value: 'Geo'
  },
  {
    label: 'Ranchers',
    value: 'Ranchers'
  },
  {
    label: 'Sonsie One',
    value: 'Sonsie+One'
  },
  {
    label: 'Kotta One',
    value: 'Kotta+One'
  },
  {
    label: 'Kosugi',
    value: 'Kosugi'
  },
  {
    label: 'Mandali',
    value: 'Mandali'
  },
  {
    label: 'Charm',
    value: 'Charm'
  },
  {
    label: 'Aguafina Script',
    value: 'Aguafina+Script'
  },
  {
    label: 'Alata',
    value: 'Alata'
  },
  {
    label: 'Imprima',
    value: 'Imprima'
  },
  {
    label: 'The Girl Next Door',
    value: 'The+Girl+Next+Door'
  },
  {
    label: 'Slackey',
    value: 'Slackey'
  },
  {
    label: 'Loved by the King',
    value: 'Loved+by+the+King'
  },
  {
    label: 'Over the Rainbow',
    value: 'Over+the+Rainbow'
  },
  {
    label: 'Euphoria Script',
    value: 'Euphoria+Script'
  },
  {
    label: 'Shanti',
    value: 'Shanti'
  },
  {
    label: 'Codystar',
    value: 'Codystar'
  },
  {
    label: 'Port Lligat Sans',
    value: 'Port+Lligat+Sans'
  },
  {
    label: 'Ruluko',
    value: 'Ruluko'
  },
  {
    label: 'Timmana',
    value: 'Timmana'
  },
  {
    label: 'Sura',
    value: 'Sura'
  },
  {
    label: 'Meddon',
    value: 'Meddon'
  },
  {
    label: 'Encode Sans Expanded',
    value: 'Encode+Sans+Expanded'
  },
  {
    label: 'Orienta',
    value: 'Orienta'
  },
  {
    label: 'Nosifer',
    value: 'Nosifer'
  },
  {
    label: 'Sarpanch',
    value: 'Sarpanch'
  },
  {
    label: 'Englebert',
    value: 'Englebert'
  },
  {
    label: 'Gafata',
    value: 'Gafata'
  },
  {
    label: 'Bilbo Swash Caps',
    value: 'Bilbo+Swash+Caps'
  },
  {
    label: 'Vibur',
    value: 'Vibur'
  },
  {
    label: 'Metamorphous',
    value: 'Metamorphous'
  },
  {
    label: 'Manuale',
    value: 'Manuale'
  },
  {
    label: 'Ledger',
    value: 'Ledger'
  },
  {
    label: 'Bilbo',
    value: 'Bilbo'
  },
  {
    label: 'Srisakdi',
    value: 'Srisakdi'
  },
  {
    label: 'Princess Sofia',
    value: 'Princess+Sofia'
  },
  {
    label: 'Dokdo',
    value: 'Dokdo'
  },
  {
    label: 'Creepster',
    value: 'Creepster'
  },
  {
    label: 'Dekko',
    value: 'Dekko'
  },
  {
    label: 'Rationale',
    value: 'Rationale'
  },
  {
    label: 'Zilla Slab Highlight',
    value: 'Zilla+Slab+Highlight'
  },
  {
    label: 'Lily Script One',
    value: 'Lily+Script+One'
  },
  {
    label: 'Alatsi',
    value: 'Alatsi'
  },
  {
    label: 'Farsan',
    value: 'Farsan'
  },
  {
    label: 'Hepta Slab',
    value: 'Hepta+Slab'
  },
  {
    label: 'Cherry Cream Soda',
    value: 'Cherry+Cream+Soda'
  },
  {
    label: 'Pavanam',
    value: 'Pavanam'
  },
  {
    label: 'Koulen',
    value: 'Koulen'
  },
  {
    label: 'Medula One',
    value: 'Medula+One'
  },
  {
    label: 'Darker Grotesque',
    value: 'Darker+Grotesque'
  },
  {
    label: 'Mukta Mahee',
    value: 'Mukta+Mahee'
  },
  {
    label: 'Salsa',
    value: 'Salsa'
  },
  {
    label: 'Antic Didone',
    value: 'Antic+Didone'
  },
  {
    label: 'Wire One',
    value: 'Wire+One'
  },
  {
    label: 'Germania One',
    value: 'Germania+One'
  },
  {
    label: 'Just Me Again Down Here',
    value: 'Just+Me+Again+Down+Here'
  },
  {
    label: 'Saira Stencil One',
    value: 'Saira+Stencil+One'
  },
  {
    label: 'David Libre',
    value: 'David+Libre'
  },
  {
    label: 'Engagement',
    value: 'Engagement'
  },
  {
    label: 'Nokora',
    value: 'Nokora'
  },
  {
    label: 'Prosto One',
    value: 'Prosto+One'
  },
  {
    label: 'Averia Libre',
    value: 'Averia+Libre'
  },
  {
    label: 'Nova Mono',
    value: 'Nova+Mono'
  },
  {
    label: 'Libre Barcode 39',
    value: 'Libre+Barcode+39'
  },
  {
    label: 'Crushed',
    value: 'Crushed'
  },
  {
    label: 'Fascinate Inline',
    value: 'Fascinate+Inline'
  },
  {
    label: 'Akronim',
    value: 'Akronim'
  },
  {
    label: 'Sirin Stencil',
    value: 'Sirin+Stencil'
  },
  {
    label: 'Bubbler One',
    value: 'Bubbler+One'
  },
  {
    label: 'Text Me One',
    value: 'Text+Me+One'
  },
  {
    label: 'Ruslan Display',
    value: 'Ruslan+Display'
  },
  {
    label: 'Vollkorn SC',
    value: 'Vollkorn+SC'
  },
  {
    label: 'Elsie',
    value: 'Elsie'
  },
  {
    label: 'Sail',
    value: 'Sail'
  },
  {
    label: 'Sumana',
    value: 'Sumana'
  },
  {
    label: 'Gaegu',
    value: 'Gaegu'
  },
  {
    label: 'Grenze',
    value: 'Grenze'
  },
  {
    label: 'Peralta',
    value: 'Peralta'
  },
  {
    label: 'Port Lligat Slab',
    value: 'Port+Lligat+Slab'
  },
  {
    label: 'Ranga',
    value: 'Ranga'
  },
  {
    label: 'Big Shoulders Text',
    value: 'Big+Shoulders+Text'
  },
  {
    label: 'Vampiro One',
    value: 'Vampiro+One'
  },
  {
    label: 'Jacques Francois Shadow',
    value: 'Jacques+Francois+Shadow'
  },
  {
    label: 'Tulpen One',
    value: 'Tulpen+One'
  },
  {
    label: 'Ribeye',
    value: 'Ribeye'
  },
  {
    label: 'Chau Philomene One',
    value: 'Chau+Philomene+One'
  },
  {
    label: 'Khmer',
    value: 'Khmer'
  },
  {
    label: 'Amarante',
    value: 'Amarante'
  },
  {
    label: 'Ewert',
    value: 'Ewert'
  },
  {
    label: 'Asul',
    value: 'Asul'
  },
  {
    label: 'Scope One',
    value: 'Scope+One'
  },
  {
    label: 'Baskervville',
    value: 'Baskervville'
  },
  {
    label: 'Ribeye Marrow',
    value: 'Ribeye+Marrow'
  },
  {
    label: 'Bayon',
    value: 'Bayon'
  },
  {
    label: 'Chicle',
    value: 'Chicle'
  },
  {
    label: 'Kite One',
    value: 'Kite+One'
  },
  {
    label: 'Ma Shan Zheng',
    value: 'Ma+Shan+Zheng'
  },
  {
    label: 'Kranky',
    value: 'Kranky'
  },
  {
    label: 'Habibi',
    value: 'Habibi'
  },
  {
    label: 'IM Fell English SC',
    value: 'IM+Fell+English+SC'
  },
  {
    label: 'Trochut',
    value: 'Trochut'
  },
  {
    label: 'Henny Penny',
    value: 'Henny+Penny'
  },
  {
    label: 'Mansalva',
    value: 'Mansalva'
  },
  {
    label: 'Baloo Thambi 2',
    value: 'Baloo+Thambi+2'
  },
  {
    label: 'Mystery Quest',
    value: 'Mystery+Quest'
  },
  {
    label: 'Modak',
    value: 'Modak'
  },
  {
    label: 'Arya',
    value: 'Arya'
  },
  {
    label: 'Monsieur La Doulaise',
    value: 'Monsieur+La+Doulaise'
  },
  {
    label: 'Quintessential',
    value: 'Quintessential'
  },
  {
    label: 'Manjari',
    value: 'Manjari'
  },
  {
    label: 'Sarina',
    value: 'Sarina'
  },
  {
    label: 'Almendra',
    value: 'Almendra'
  },
  {
    label: 'League Script',
    value: 'League+Script'
  },
  {
    label: 'Dynalight',
    value: 'Dynalight'
  },
  {
    label: 'Unlock',
    value: 'Unlock'
  },
  {
    label: 'Miniver',
    value: 'Miniver'
  },
  {
    label: 'Mate SC',
    value: 'Mate+SC'
  },
  {
    label: 'Barrio',
    value: 'Barrio'
  },
  {
    label: 'Mogra',
    value: 'Mogra'
  },
  {
    label: 'Hanalei Fill',
    value: 'Hanalei+Fill'
  },
  {
    label: 'Flamenco',
    value: 'Flamenco'
  },
  {
    label: 'Griffy',
    value: 'Griffy'
  },
  {
    label: 'Dorsa',
    value: 'Dorsa'
  },
  {
    label: 'Chathura',
    value: 'Chathura'
  },
  {
    label: 'Ramaraja',
    value: 'Ramaraja'
  },
  {
    label: 'ZCOOL QingKe HuangYou',
    value: 'ZCOOL+QingKe+HuangYou'
  },
  {
    label: 'Rosarivo',
    value: 'Rosarivo'
  },
  {
    label: 'Pirata One',
    value: 'Pirata+One'
  },
  {
    label: 'Chilanka',
    value: 'Chilanka'
  },
  {
    label: 'New Rocker',
    value: 'New+Rocker'
  },
  {
    label: 'Coiny',
    value: 'Coiny'
  },
  {
    label: 'Ibarra Real Nova',
    value: 'Ibarra+Real+Nova'
  },
  {
    label: 'Kumar One',
    value: 'Kumar+One'
  },
  {
    label: 'Mina',
    value: 'Mina'
  },
  {
    label: 'Italiana',
    value: 'Italiana'
  },
  {
    label: 'B612',
    value: 'B612'
  },
  {
    label: 'Chango',
    value: 'Chango'
  },
  {
    label: 'Angkor',
    value: 'Angkor'
  },
  {
    label: 'Milonga',
    value: 'Milonga'
  },
  {
    label: 'Vibes',
    value: 'Vibes'
  },
  {
    label: 'Crimson Pro',
    value: 'Crimson+Pro'
  },
  {
    label: 'Sree Krushnadevaraya',
    value: 'Sree+Krushnadevaraya'
  },
  {
    label: 'Averia Gruesa Libre',
    value: 'Averia+Gruesa+Libre'
  },
  {
    label: 'Lovers Quarrel',
    value: 'Lovers+Quarrel'
  },
  {
    label: 'Overlock SC',
    value: 'Overlock+SC'
  },
  {
    label: 'Fenix',
    value: 'Fenix'
  },
  {
    label: 'Eagle Lake',
    value: 'Eagle+Lake'
  },
  {
    label: 'Inknut Antiqua',
    value: 'Inknut+Antiqua'
  },
  {
    label: 'Kantumruy',
    value: 'Kantumruy'
  },
  {
    label: 'Meera Inimai',
    value: 'Meera+Inimai'
  },
  {
    label: 'IM Fell Great Primer SC',
    value: 'IM+Fell+Great+Primer+SC'
  },
  {
    label: 'Donegal One',
    value: 'Donegal+One'
  },
  {
    label: 'Paprika',
    value: 'Paprika'
  },
  {
    label: 'Condiment',
    value: 'Condiment'
  },
  {
    label: 'Kavoon',
    value: 'Kavoon'
  },
  {
    label: 'Barriecito',
    value: 'Barriecito'
  },
  {
    label: 'Simonetta',
    value: 'Simonetta'
  },
  {
    label: 'Nova Round',
    value: 'Nova+Round'
  },
  {
    label: 'Thasadith',
    value: 'Thasadith'
  },
  {
    label: 'Big Shoulders Display',
    value: 'Big+Shoulders+Display'
  },
  {
    label: 'IM Fell DW Pica SC',
    value: 'IM+Fell+DW+Pica+SC'
  },
  {
    label: 'Dangrek',
    value: 'Dangrek'
  },
  {
    label: 'Petrona',
    value: 'Petrona'
  },
  {
    label: 'Stint Ultra Expanded',
    value: 'Stint+Ultra+Expanded'
  },
  {
    label: 'Sancreek',
    value: 'Sancreek'
  },
  {
    label: 'Cagliostro',
    value: 'Cagliostro'
  },
  {
    label: 'Offside',
    value: 'Offside'
  },
  {
    label: 'Prociono',
    value: 'Prociono'
  },
  {
    label: 'Stalemate',
    value: 'Stalemate'
  },
  {
    label: 'Notable',
    value: 'Notable'
  },
  {
    label: 'Maiden Orange',
    value: 'Maiden+Orange'
  },
  {
    label: 'Delius Unicase',
    value: 'Delius+Unicase'
  },
  {
    label: 'Junge',
    value: 'Junge'
  },
  {
    label: 'Julee',
    value: 'Julee'
  },
  {
    label: 'Stint Ultra Condensed',
    value: 'Stint+Ultra+Condensed'
  },
  {
    label: 'IM Fell French Canon',
    value: 'IM+Fell+French+Canon'
  },
  {
    label: 'Stoke',
    value: 'Stoke'
  },
  {
    label: 'Marko One',
    value: 'Marko+One'
  },
  {
    label: 'Be Vietnam',
    value: 'Be+Vietnam'
  },
  {
    label: 'Londrina Outline',
    value: 'Londrina+Outline'
  },
  {
    label: 'Swanky and Moo Moo',
    value: 'Swanky+and+Moo+Moo'
  },
  {
    label: 'Blinker',
    value: 'Blinker'
  },
  {
    label: 'Oxanium',
    value: 'Oxanium'
  },
  {
    label: 'Kodchasan',
    value: 'Kodchasan'
  },
  {
    label: 'UnifrakturCook',
    value: 'UnifrakturCook'
  },
  {
    label: 'IM Fell Great Primer',
    value: 'IM+Fell+Great+Primer'
  },
  {
    label: 'Asar',
    value: 'Asar'
  },
  {
    label: 'Rum Raisin',
    value: 'Rum+Raisin'
  },
  {
    label: 'Moul',
    value: 'Moul'
  },
  {
    label: 'Montaga',
    value: 'Montaga'
  },
  {
    label: 'Uncial Antiqua',
    value: 'Uncial+Antiqua'
  },
  {
    label: 'Snippet',
    value: 'Snippet'
  },
  {
    label: 'Tillana',
    value: 'Tillana'
  },
  {
    label: 'Margarine',
    value: 'Margarine'
  },
  {
    label: 'Autour One',
    value: 'Autour+One'
  },
  {
    label: 'Flavors',
    value: 'Flavors'
  },
  {
    label: 'Farro',
    value: 'Farro'
  },
  {
    label: 'Bokor',
    value: 'Bokor'
  },
  {
    label: 'Redressed',
    value: 'Redressed'
  },
  {
    label: 'Libre Caslon Text',
    value: 'Libre+Caslon+Text'
  },
  {
    label: 'Tomorrow',
    value: 'Tomorrow'
  },
  {
    label: 'Underdog',
    value: 'Underdog'
  },
  {
    label: 'Nova Slim',
    value: 'Nova+Slim'
  },
  {
    label: 'Diplomata SC',
    value: 'Diplomata+SC'
  },
  {
    label: 'Fira Code',
    value: 'Fira+Code'
  },
  {
    label: 'Odor Mean Chey',
    value: 'Odor+Mean+Chey'
  },
  {
    label: 'Diplomata',
    value: 'Diplomata'
  },
  {
    label: 'Calistoga',
    value: 'Calistoga'
  },
  {
    label: 'Della Respira',
    value: 'Della+Respira'
  },
  {
    label: 'Nova Cut',
    value: 'Nova+Cut'
  },
  {
    label: 'Buda',
    value: 'Buda'
  },
  {
    label: 'Galdeano',
    value: 'Galdeano'
  },
  {
    label: 'Cormorant Unicase',
    value: 'Cormorant+Unicase'
  },
  {
    label: 'Rhodium Libre',
    value: 'Rhodium+Libre'
  },
  {
    label: 'Bigshot One',
    value: 'Bigshot+One'
  },
  {
    label: 'Snowburst One',
    value: 'Snowburst+One'
  },
  {
    label: 'Siemreap',
    value: 'Siemreap'
  },
  {
    label: 'Chela One',
    value: 'Chela+One'
  },
  {
    label: 'Baloo Da 2',
    value: 'Baloo+Da+2'
  },
  {
    label: 'Gamja Flower',
    value: 'Gamja+Flower'
  },
  {
    label: 'Glass Antiqua',
    value: 'Glass+Antiqua'
  },
  {
    label: 'Poller One',
    value: 'Poller+One'
  },
  {
    label: 'Elsie Swash Caps',
    value: 'Elsie+Swash+Caps'
  },
  {
    label: 'Song Myung',
    value: 'Song+Myung'
  },
  {
    label: 'Comic Neue',
    value: 'Comic+Neue'
  },
  {
    label: 'Lakki Reddy',
    value: 'Lakki+Reddy'
  },
  {
    label: 'Stylish',
    value: 'Stylish'
  },
  {
    label: 'Trykker',
    value: 'Trykker'
  },
  {
    label: 'Nova Flat',
    value: 'Nova+Flat'
  },
  {
    label: 'Jim Nightshade',
    value: 'Jim+Nightshade'
  },
  {
    label: 'Inika',
    value: 'Inika'
  },
  {
    label: 'Original Surfer',
    value: 'Original+Surfer'
  },
  {
    label: 'Linden Hill',
    value: 'Linden+Hill'
  },
  {
    label: 'Mrs Sheppards',
    value: 'Mrs+Sheppards'
  },
  {
    label: 'Content',
    value: 'Content'
  },
  {
    label: 'Revalia',
    value: 'Revalia'
  },
  {
    label: 'Modern Antiqua',
    value: 'Modern+Antiqua'
  },
  {
    label: 'Joti One',
    value: 'Joti+One'
  },
  {
    label: 'Wellfleet',
    value: 'Wellfleet'
  },
  {
    label: 'Charmonman',
    value: 'Charmonman'
  },
  {
    label: 'Felipa',
    value: 'Felipa'
  },
  {
    label: 'Meie Script',
    value: 'Meie+Script'
  },
  {
    label: 'Metal Mania',
    value: 'Metal+Mania'
  },
  {
    label: 'Oldenburg',
    value: 'Oldenburg'
  },
  {
    label: 'MedievalSharp',
    value: 'MedievalSharp'
  },
  {
    label: 'Sedgwick Ave Display',
    value: 'Sedgwick+Ave+Display'
  },
  {
    label: 'Monofett',
    value: 'Monofett'
  },
  {
    label: 'Fahkwang',
    value: 'Fahkwang'
  },
  {
    label: 'Ruthie',
    value: 'Ruthie'
  },
  {
    label: 'KoHo',
    value: 'KoHo'
  },
  {
    label: 'Smythe',
    value: 'Smythe'
  },
  {
    label: 'Plaster',
    value: 'Plaster'
  },
  {
    label: 'Suwannaphum',
    value: 'Suwannaphum'
  },
  {
    label: 'Jomhuria',
    value: 'Jomhuria'
  },
  {
    label: 'Caesar Dressing',
    value: 'Caesar+Dressing'
  },
  {
    label: 'Galindo',
    value: 'Galindo'
  },
  {
    label: 'Irish Grover',
    value: 'Irish+Grover'
  },
  {
    label: 'Hi Melody',
    value: 'Hi+Melody'
  },
  {
    label: 'Gotu',
    value: 'Gotu'
  },
  {
    label: 'Risque',
    value: 'Risque'
  },
  {
    label: 'Sahitya',
    value: 'Sahitya'
  },
  {
    label: 'Girassol',
    value: 'Girassol'
  },
  {
    label: 'Taprom',
    value: 'Taprom'
  },
  {
    label: 'Bahiana',
    value: 'Bahiana'
  },
  {
    label: 'Gorditas',
    value: 'Gorditas'
  },
  {
    label: 'Ravi Prakash',
    value: 'Ravi+Prakash'
  },
  {
    label: 'Libre Barcode 39 Extended Text',
    value: 'Libre+Barcode+39+Extended+Text'
  },
  {
    label: 'Croissant One',
    value: 'Croissant+One'
  },
  {
    label: 'Purple Purse',
    value: 'Purple+Purse'
  },
  {
    label: 'Lancelot',
    value: 'Lancelot'
  },
  {
    label: 'Cute Font',
    value: 'Cute+Font'
  },
  {
    label: 'Kdam Thmor',
    value: 'Kdam+Thmor'
  },
  {
    label: 'Libre Barcode 128',
    value: 'Libre+Barcode+128'
  },
  {
    label: 'Devonshire',
    value: 'Devonshire'
  },
  {
    label: 'Arbutus',
    value: 'Arbutus'
  },
  {
    label: 'Major Mono Display',
    value: 'Major+Mono+Display'
  },
  {
    label: 'Jacques Francois',
    value: 'Jacques+Francois'
  },
  {
    label: 'Odibee Sans',
    value: 'Odibee+Sans'
  },
  {
    label: 'Libre Caslon Display',
    value: 'Libre+Caslon+Display'
  },
  {
    label: 'Almendra SC',
    value: 'Almendra+SC'
  },
  {
    label: 'Londrina Shadow',
    value: 'Londrina+Shadow'
  },
  {
    label: 'Yeon Sung',
    value: 'Yeon+Sung'
  },
  {
    label: 'Keania One',
    value: 'Keania+One'
  },
  {
    label: 'Iceberg',
    value: 'Iceberg'
  },
  {
    label: 'Asset',
    value: 'Asset'
  },
  {
    label: 'Atomic Age',
    value: 'Atomic+Age'
  },
  {
    label: 'Poor Story',
    value: 'Poor+Story'
  },
  {
    label: 'Kulim Park',
    value: 'Kulim+Park'
  },
  {
    label: 'Libre Barcode 39 Text',
    value: 'Libre+Barcode+39+Text'
  },
  {
    label: 'Kirang Haerang',
    value: 'Kirang+Haerang'
  },
  {
    label: 'Ruge Boogie',
    value: 'Ruge+Boogie'
  },
  {
    label: 'East Sea Dokdo',
    value: 'East+Sea+Dokdo'
  },
  {
    label: 'Gupter',
    value: 'Gupter'
  },
  {
    label: 'Goblin One',
    value: 'Goblin+One'
  },
  {
    label: 'GFS Neohellenic',
    value: 'GFS+Neohellenic'
  },
  {
    label: 'Piedra',
    value: 'Piedra'
  },
  {
    label: 'Molle',
    value: 'Molle'
  },
  {
    label: 'Dr Sugiyama',
    value: 'Dr+Sugiyama'
  },
  {
    label: 'Freehand',
    value: 'Freehand'
  },
  {
    label: 'Romanesco',
    value: 'Romanesco'
  },
  {
    label: 'Supermercado One',
    value: 'Supermercado+One'
  },
  {
    label: 'Smokum',
    value: 'Smokum'
  },
  {
    label: 'Kavivanar',
    value: 'Kavivanar'
  },
  {
    label: 'Geostar Fill',
    value: 'Geostar+Fill'
  },
  {
    label: 'Peddana',
    value: 'Peddana'
  },
  {
    label: 'Moulpali',
    value: 'Moulpali'
  },
  {
    label: 'Almendra Display',
    value: 'Almendra+Display'
  },
  {
    label: 'Bonbon',
    value: 'Bonbon'
  },
  {
    label: 'Metal',
    value: 'Metal'
  },
  {
    label: 'Libre Barcode 39 Extended',
    value: 'Libre+Barcode+39+Extended'
  },
  {
    label: 'Lexend Exa',
    value: 'Lexend+Exa'
  },
  {
    label: 'Lacquer',
    value: 'Lacquer'
  },
  {
    label: 'Hanalei',
    value: 'Hanalei'
  },
  {
    label: 'Sunshiney',
    value: 'Sunshiney'
  },
  {
    label: 'Seymour One',
    value: 'Seymour+One'
  },
  {
    label: 'Sevillana',
    value: 'Sevillana'
  },
  {
    label: 'Courier Prime',
    value: 'Courier+Prime'
  },
  {
    label: 'Beth Ellen',
    value: 'Beth+Ellen'
  },
  {
    label: 'Kumar One Outline',
    value: 'Kumar+One+Outline'
  },
  {
    label: 'Tenali Ramakrishna',
    value: 'Tenali+Ramakrishna'
  },
  {
    label: 'IM Fell Double Pica SC',
    value: 'IM+Fell+Double+Pica+SC'
  },
  {
    label: 'Miss Fajardose',
    value: 'Miss+Fajardose'
  },
  {
    label: 'Bellota',
    value: 'Bellota'
  },
  {
    label: 'Preahvihear',
    value: 'Preahvihear'
  },
  {
    label: 'Butterfly Kids',
    value: 'Butterfly+Kids'
  },
  {
    label: 'Bellota Text',
    value: 'Bellota+Text'
  },
  {
    label: 'Jolly Lodger',
    value: 'Jolly+Lodger'
  },
  {
    label: 'Emblema One',
    value: 'Emblema+One'
  },
  {
    label: 'Astloch',
    value: 'Astloch'
  },
  {
    label: 'Libre Barcode 128 Text',
    value: 'Libre+Barcode+128+Text'
  },
  {
    label: 'Butcherman',
    value: 'Butcherman'
  },
  {
    label: 'Macondo',
    value: 'Macondo'
  },
  {
    label: 'ZCOOL KuaiLe',
    value: 'ZCOOL+KuaiLe'
  },
  {
    label: 'Caladea',
    value: 'Caladea'
  },
  {
    label: 'Mr Bedfort',
    value: 'Mr+Bedfort'
  },
  {
    label: 'Bungee Outline',
    value: 'Bungee+Outline'
  },
  {
    label: 'Black And White Picture',
    value: 'Black+And+White+Picture'
  },
  {
    label: 'Inria Serif',
    value: 'Inria+Serif'
  },
  {
    label: 'Miltonian Tattoo',
    value: 'Miltonian+Tattoo'
  },
  {
    label: 'Combo',
    value: 'Combo'
  },
  {
    label: 'Nova Oval',
    value: 'Nova+Oval'
  },
  {
    label: 'Erica One',
    value: 'Erica+One'
  },
  {
    label: 'Londrina Sketch',
    value: 'Londrina+Sketch'
  },
  {
    label: 'Nova Script',
    value: 'Nova+Script'
  },
  {
    label: 'Sofadi One',
    value: 'Sofadi+One'
  },
  {
    label: 'Fruktur',
    value: 'Fruktur'
  },
  {
    label: 'Fascinate',
    value: 'Fascinate'
  },
  {
    label: 'Bungee Hairline',
    value: 'Bungee+Hairline'
  },
  {
    label: 'Gidugu',
    value: 'Gidugu'
  },
  {
    label: 'Gayathri',
    value: 'Gayathri'
  },
  {
    label: 'Passero One',
    value: 'Passero+One'
  },
  {
    label: 'Aubrey',
    value: 'Aubrey'
  },
  {
    label: 'Bigelow Rules',
    value: 'Bigelow+Rules'
  },
  {
    label: 'Inria Sans',
    value: 'Inria+Sans'
  },
  {
    label: 'Stalinist One',
    value: 'Stalinist+One'
  },
  {
    label: 'Solway',
    value: 'Solway'
  },
  {
    label: 'Miltonian',
    value: 'Miltonian'
  },
  {
    label: 'Geostar',
    value: 'Geostar'
  },
  {
    label: 'Sulphur Point',
    value: 'Sulphur+Point'
  },
  {
    label: 'Federant',
    value: 'Federant'
  },
  {
    label: 'Chenla',
    value: 'Chenla'
  },
  {
    label: 'Zhi Mang Xing',
    value: 'Zhi+Mang+Xing'
  },
  {
    label: 'Suravaram',
    value: 'Suravaram'
  },
  {
    label: 'Kenia',
    value: 'Kenia'
  },
  {
    label: 'Fasthand',
    value: 'Fasthand'
  },
  {
    label: 'Dhurjati',
    value: 'Dhurjati'
  },
  {
    label: 'BioRhyme Expanded',
    value: 'BioRhyme+Expanded'
  },
  {
    label: 'Single Day',
    value: 'Single+Day'
  },
  {
    label: 'Lexend Tera',
    value: 'Lexend+Tera'
  },
  {
    label: 'Baloo Bhai 2',
    value: 'Baloo+Bhai+2'
  },
  {
    label: 'Warnes',
    value: 'Warnes'
  },
  {
    label: 'Turret Road',
    value: 'Turret+Road'
  },
  {
    label: 'Long Cang',
    value: 'Long+Cang'
  },
  {
    label: 'Viaoda Libre',
    value: 'Viaoda+Libre'
  },
  {
    label: 'Lexend Giga',
    value: 'Lexend+Giga'
  },
  {
    label: 'Lexend Mega',
    value: 'Lexend+Mega'
  },
  {
    label: 'Lexend Zetta',
    value: 'Lexend+Zetta'
  },
  {
    label: 'Baloo Tammudu 2',
    value: 'Baloo+Tammudu+2'
  },
  {
    label: 'Bahianita',
    value: 'Bahianita'
  },
  {
    label: 'Lexend Peta',
    value: 'Lexend+Peta'
  },
  {
    label: 'Baloo Bhaina 2',
    value: 'Baloo+Bhaina+2'
  },
  {
    label: 'Baloo Paaji 2',
    value: 'Baloo+Paaji+2'
  },
  {
    label: 'Baloo Tamma 2',
    value: 'Baloo+Tamma+2'
  }
];

export default allFonts;
