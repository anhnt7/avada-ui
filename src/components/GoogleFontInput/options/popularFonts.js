export const popularFonts = [
  {label: 'Roboto', value: 'Roboto'},
  {label: 'Open Sans', value: 'Open+Sans'},
  {label: 'Lato', value: 'Lato'},
  {label: 'Lobster', value: 'Lobster'},
  {label: 'Oswald', value: 'Oswald'},
  {label: 'Dancing Script', value: 'Dancing+Script'},
  {label: 'Montserrat', value: 'Montserrat'},
  {label: 'EB Garamond', value: 'EB+Garamond'},
  {label: 'Anton', value: 'Anton'},
  {label: 'Josefin Sans', value: 'Josefin+Sans'},
  {label: 'Indie Flower', value: 'Indie+Flower'},
  {label: 'Shadows Into Light', value: 'Shadows+Into+Light'},
  {label: 'Caveat', value: 'Caveat'},
  {label: 'Playfair display', value: 'Playfair+Display'}
];
