import React, {useState} from 'react';
import {Button, Modal, Stack, Tooltip} from "@shopify/polaris";
import PropTypes from 'prop-types';
import {CirclePlusMajorMonotone} from "@shopify/polaris-icons";
import PopularFontSelectList from "./PopularFontSelectList/PopularFontSelectList";
import FontList from "./FontList/FontList";

/**
 * Google Font Input
 *
 * @param onSelect
 * @param value
 * @param label
 * @returns {*}
 * @constructor
 */
const GoogleFontInput = ({onSelect, value, label}) => {
  const [openModal, setOpenModal] = useState(false);

  return (
    <React.Fragment>
      <Stack>
        <PopularFontSelectList
          label={label}
          onSelect={onSelect}
          value={value}
        />
        <Tooltip content={"More fonts"}>
          <div style={{marginTop: "3.2rem", marginLeft: "-2rem"}}>
            <Button plain icon={CirclePlusMajorMonotone} onClick={() => setOpenModal(true)} />
          </div>
        </Tooltip>
      </Stack>
      <Modal
        open={openModal}
        large
        title={"Font Manager"}
        onClose={() => {
          setOpenModal(false)
        }}
      >
        <Modal.Section>
          <FontList onSelect={(val) => {
            setOpenModal(false);
            onSelect(val)
          }}/>
        </Modal.Section>
      </Modal>
    </React.Fragment>
  )
};

GoogleFontInput.propTypes = {
  /** Handler when select a font */
  onSelect: PropTypes.func.isRequired,
  /** The current selected font */
  value: PropTypes.string.isRequired,
  /** The label for the input. Default is "Font family" */
  label: PropTypes.string
};

GoogleFontInput.defaultProps = {
  label: "Font family"
};

export default GoogleFontInput;
